<?php
$ret = [];
if( WP_ENV != 'production' ) {
 
  $ret = [
      'type' => 'cpt',
      'name'                => __( 'Project', 'text-domain' ),
      'singular_name'       => __( 'Project', 'text-domain' ),
      'menu_name'           => __( 'Project', 'text-domain' ),

      'supports' => [
        'title',
        'editor',
        'page-attributes', 
        'author', 
        'thumbnail',
        'custom-fields', 
        'post-formats', 
        'comments',
        'page-attributes'
      ],
      'config' => [
        'hierarchical' => true,
        'menu_icon' => 'dashicons-format-aside',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'description'         => '',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'rewrite'             => array('slug' => 'project'),
        'capability_type'     => 'post',
      ],
  ];
}

return $ret;

