<?php

$context = Timber::get_context();
$context['page'] = new TimberPost();
$context['post'] = Timber::get_post();
$context['projects'] = Timber::get_posts(array(
    'post_type' => 'project',
    'order' => 'ASC',
    'orderby' => 'date'
));
$context['medias'] = Timber::get_posts(array(
    'post_type' => 'mediasocial',
    'order' => 'ASC',
    'orderby' => 'date'
));
$context['categories'] = Timber::get_terms('projects');
$context['aboutus'] = Timber::get_posts(array(
    'post_type' => 'aboutus',
    'order' => 'ASC',
    'orderby' => 'date'
));
$context['teams'] = Timber::get_posts(array(
    'post_type' => 'team',
    'order' => 'ASC',
    'orderby' => 'date'
));


Timber::render('views/templates/home/template.twig', $context);