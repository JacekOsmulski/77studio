import $ from 'jquery';
export const addClassActiveMenu = () => {
  const thumbsTeam = document.querySelectorAll('.team__thumb');

  thumbsTeam.forEach(thumb => {
    $(thumb).on('click', e => {
      //wrap element
      if (thumb.lastElementChild.nodeName === 'IMG') return;
      const teamWrap = e.target.lastElementChild;
      const targetElement = e.target;
      const activeHiddenContent = e.target.insertAdjacentElement(
        'beforebegin',
        teamWrap
      );
      //run Code
      function showTeamContent() {
        thumbsTeam.forEach((elem, index) => {
          $('.team__thumb').removeClass('activeAnim');
          $('.team__thumb').addClass('blockItem');
          $('.team__wrap').removeClass('active');
          teamWrap.classList.add('active');
          elem.classList.add('overlay');
          targetElement.classList.add('activeAnim');

          if (targetElement.classList.contains('activeAnim')) {
            targetElement.insertAdjacentElement('beforebegin', teamWrap);
            $('.team__thumb').removeClass('show');
            e.target.classList.add('show');
            if (teamWrap) teamWrap.classList.add('active');
            //Close single thumbs
            $('.team__close').on('click', e => {
              e.stopPropagation();
              targetElement.insertAdjacentElement(
                'beforeend',
                activeHiddenContent
              );
              $('.team__thumb').removeClass(
                'slideInDown',
                'animated',
                'slideInUp',
                'activeAnim'
              );
              $('.team__wrap').removeClass('active');
              $('.team__thumb').addClass('show');
            });
          }
        });
      }

      showTeamContent();
    });

    $('.team__close').on('click', e => {
      $('.team__thumb').removeClass('blockItem');
    });
  });
};
