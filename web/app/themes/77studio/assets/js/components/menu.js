// After scroll page >= 100 hamburger btn hidden, show menu
export const switchMenu = () => {
  let isThrottled = false;
  const headerSwitch = document.querySelector('.header__switch');
  const switchClose = document.querySelector('.header__close');
  const subMenu = document.querySelector('#submenu');

  const subMenuLinks = document.querySelectorAll('.header__submenu-href');
  const menuLinks = document.querySelectorAll('.header__nav-link');

  const removeBlockScrollSection = () => {
    menuLinks.forEach(elem => {
      elem.addEventListener('click', e => {
        fullpage_api.setAllowScrolling(true);
      });
    });
  };

  const removeActiveClassSubLink = () => {
    subMenuLinks.forEach(elem => {
      elem.addEventListener('click', e => {
        if (window.innerWidth <= 992) {
          headerSwitch.classList.remove('open');
        }
        subMenuLinks.forEach(subLink => {
          subLink.classList.remove('active');
          subMenu.classList.add('active');
        });
        e.target.classList.add('active');
      });
    });
  };

  headerSwitch.addEventListener('click', e => {
    e.preventDefault();
    e.target.classList.add('open');

    if (document.querySelector('.portfolio.active')) {
      subMenu.classList.add('active');
    }
  });

  switchClose.addEventListener('click', () => {
    headerSwitch.classList.remove('open');
  });

  document.addEventListener('wheel', e => {
    if (isThrottled) return;
    isThrottled = true;

    setTimeout(() => {
      isThrottled = false;
    }, 1000);

    if (window.innerWidth >= 992) {
      headerSwitch.classList.add('open');
    }
  });

  removeBlockScrollSection();
  removeActiveClassSubLink();
};
