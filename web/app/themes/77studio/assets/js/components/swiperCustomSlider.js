import Swiper from 'swiper';

const mySwiper = new Swiper('.teamLong', {
  slidesPerView: 'auto',
  breakpointsInverse: true,
  direction: 'vertical',
});

class SwiperCustomSlider {
  constructor(mountSelector) {
    this.slides = '.slide__thumb';
    this.mountSelector = document.querySelectorAll(mountSelector);
    this.renderSlider();
    this.menuLinks = document.querySelectorAll('.header__nav-link');
  }

  renderSlider() {
    this.mountSelector.forEach(thumb => {
      thumb.addEventListener('click', e => {
        this.mountSelector.forEach((elem, index) => {
          elem.classList.add('animated', 'zoomOutRight');

          if (index === 0) {
            elem.classList.add('delay-1-1s');
          }
          if (index === 1) {
            elem.classList.add('delay-1-2s');
          }
          if (index === 2) {
            elem.classList.add('delay-1-3s');
          }
          if (index === 4) {
            elem.classList.add('delay-1-4s');
          }
          if (index === 5) {
            elem.classList.add('delay-1-5s');
          }
          if (index === 6) {
            elem.classList.add('delay-1-6s');
          }
          if (index === 7) {
            elem.classList.add('delay-1-7s');
          }
          if (index === 8) {
            elem.classList.add('delay-1-8s');
          }
          if (index === 9) {
            elem.classList.add('delay-1-9s');
          }
        });
        if (e.target.classList.contains('slide__thumb')) {
          const headerSwitch = document.querySelector('.header__switch');
          if (window.innerWidth <= 992) {
            headerSwitch.classList.remove('open');
          } else {
            if (!thumb.classList.contains('portfolio__thumb'))
              e.target.parentElement.classList.add('fullVh');
            e.target.parentElement.parentElement.classList.add(
              'none-transform'
            );
          }

          e.target.querySelector('h3').classList.add('slide__thumb-title');
          e.target.classList.add('overlay');
          const scrollable = document.querySelector(
            'section.active .fp-scrollable'
          );
          const scroller = document.querySelector(
            'section.active .fp-scroller'
          );
          const title = document.querySelector(
            '.portfolio__thumb .slide__thumb-title'
          );
          if (e.target.classList.contains('portfolio__thumb')) {
            const titleText = document.querySelector(
              '.portfolio__thumb .slide__thumb-title'
            ).textContent;
            const submenu = document.querySelector('.header__submenu');
            const div = document.createElement('div');
            div.classList.add('header__submenu-project');
            div.innerText = titleText;
            submenu.insertAdjacentElement('afterbegin', div);
          }
          fullpage_api.setAllowScrolling(false);

          this.menuLinks.forEach(link => {
            link.addEventListener('click', e => {
              fullpage_api.setAllowScrolling(false);
              fullpage_api.setLockAnchors(true);
            });
          });

          if (scrollable) {
            scroller.style.transform = '';
          }

          e.target.insertAdjacentElement('afterend', e.target.lastElementChild);
          e.target.nextElementSibling.classList.add(
            'slide__wrap--active',
            'animated',
            'slideInRight'
          );

          setTimeout(() => {
            this.fireInstanceOfSlider(
              e.target.nextElementSibling.querySelector('.swiper-container'),
              e.target
            );
            this.closeWrap(e.target.nextElementSibling, e.target, title);
          }, 1000);
        }
      });
    });
  }

  closeWrap(elem, slide, title) {
    this.close = elem.querySelector('.ib-close');
    this.close.addEventListener('click', e => {
      e.preventDefault();
      this.menuLinks.forEach(link => {
        link.addEventListener('click', e => {
          fullpage_api.setLockAnchors(false);
          fullpage_api.setAllowScrolling(true);
          fullpage_api.setAutoScrolling(true);
        });
      });
      document.querySelectorAll(this.slides).forEach(elem => {
        elem.parentElement.classList.remove('fullVh');
      });
      if (document.querySelector('.portfolio__thumb') && title) {
        title.classList.remove('slide__thumb-title');
      }
      document
        .querySelector('.my-shuffle-container')
        .classList.remove('animated', 'zoomOut', 'zoomOutRight');
      if (document.querySelector('.header__submenu-project')) {
        document.querySelector('.header__submenu-project').remove();
      }
      const scrollable = document.querySelector(
        'section.active .fp-scrollable'
      );
      const fpScroller = document.querySelector('section.active .fp-scroller');
      if (fpScroller) {
        fpScroller.classList.remove('none-transform');
      }
      fullpage_api.setAllowScrolling(true);
      fullpage_api.setAutoScrolling(true);
      if (scrollable) {
        elem.classList.remove(
          'slide__wrap--active',
          'animated',
          'slideInRight',
          'zoomOut',
          'zoomOutRight'
        );
      } else {
        e.target.offsetParent.offsetParent.classList.remove(
          'slide__wrap--active',
          'animated',
          'slideInRight',
          'zoomOut',
          'zoomOutRight'
        );
      }

      slide.insertAdjacentElement('beforeend', elem);
    });
    document.querySelectorAll(this.slides).forEach(elem => {
      elem.classList.remove(
        'animated',
        'zoomOut',
        'fadeIn',
        'overlay',
        'fadeOut',
        'delay-1-1s',
        'delay-1-2s',
        'delay-1-3s',
        'delay-1-4s',
        'delay-1-5s',
        'delay-1-6s',
        'delay-1-7s',
        'delay-1-8s',
        'delay-1-9s',
        'zoomOutRight'
      );
    });
  }

  fireInstanceOfSlider(elem, thumb) {
    const swiperSlider = new Swiper(elem, {
      slidesPerView: 'auto',
      breakpointsInverse: true,
      direction: 'vertical',
      speed: 400,
      breakpoints: {
        992: {
          slidesPerView: 1,
          speed: 800,
          direction: 'horizontal',
          centeredSlides: true,
        },
      },
      pagination: {
        el: '.swiper__pagination',
        type: 'bullets',
        clickable: true,
      },

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      scrollbar: {
        el: 'swiper-scrollbar',
      },
      mousewheel: {
        invert: true,
      },
    });
  }
}

export default SwiperCustomSlider;
