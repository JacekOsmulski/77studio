export const categories = {
  choose: 'wybrane',
  publication: 'publiczne',
  multifamily: 'wielorodzinne',
  houses: 'domy',
  industrial: 'przemysłowe',
};
