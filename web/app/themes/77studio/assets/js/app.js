import $ from 'jquery';
import { switchMenu } from './components/menu';
import { addClassActiveMenu } from './components/aboutus';
import SwiperCustomSlider from './components/swiperCustomSlider';
import { categories } from './const/categories';
import { filterSelection } from './libs/filter';

export const fullPageScrollMobile = () => {
  // eslint-disable-next-line no-new
  const mobileScrollPage = new fullpage('#fullpage', {
    menu: '#menu',
    anchors: ['intro', 'project', 'media', 'aboutus', 'team', 'contact'],
    normalScrollElements: '.swiper-wrapper',
    controlArrows: false,
    css3: true,
    scrollingSpeed: 1000,
    fitToSection: true,
    scrollOverflow: false,
    animateAnchor: true,
    offsetSections: false,
    verticalCentered: false,
    onLeave: function(origin, destination, direction) {
      const portfolioActive = document.querySelector('.portfolio');
      const submenu = document.querySelector('.header__submenu');
      const menu = document.querySelector('.header__switch');
      if (
        origin.index === 0 &&
        direction === 'down' &&
        portfolioActive.classList.contains('active')
      ) {
        submenu.classList.add('active');
      } else if (origin.index === 1 && direction === 'up') {
        menu.classList.remove('open');
      }
    },
    afterLoad: function(origin, destination, direction) {
      const submenu = document.querySelector('.header__submenu');
      const portfolioActive = document.querySelector('.portfolio');
      const menu = document.querySelector('.header__switch');

      if (
        origin.index === 0 &&
        direction === 'down' &&
        portfolioActive.classList.contains('active')
      ) {
        filterSelection(categories.choose);
        submenu.classList.add('active');
        $(submenu)
          .find('a')
          .first()
          .addClass('active');
      } else if (
        direction === 'up' &&
        portfolioActive.classList.contains('active')
      ) {
        filterSelection(categories.choose);
        submenu.classList.add('active');
        $(submenu)
          .find('a')
          .first()
          .addClass('active');
        submenu.classList.add('active');
      } else if (origin.index === 1 && direction === 'up') {
        menu.classList.remove('open');
      }
    },
  });
};

export const fullPGSDesktop = () => {
  const desktopScrollPage = new fullpage('#fullpage', {
    menu: '#menu',
    anchors: ['intro', 'project', 'media', 'aboutus', 'team', 'contact'],
    css3: true,
    autoScrolling: true,
    scrollingSpeed: 1000,
    fitToSection: true,
    scrollOverflow: true,
    animateAnchor: true,
    offsetSections: true,
    verticalCentered: false,
    onLeave: function(origin, destination, direction) {
      const portfolioActive = document.querySelector('.portfolio');
      const submenu = document.querySelector('.header__submenu');
      const menu = document.querySelector('.header__switch');

      if (
        origin.index === 0 &&
        direction === 'down' &&
        portfolioActive.classList.contains('active')
      ) {
        submenu.classList.add('active');
      } else if (
        direction === 'up' &&
        portfolioActive.classList.contains('active')
      ) {
        submenu.classList.add('active');
      } else if (origin.index === 1 && direction === 'up') {
        menu.classList.remove('open');
      }
    },
    afterLoad: function(origin, destination, direction) {
      const submenu = document.querySelector('.header__submenu');
      const portfolioActive = document.querySelector('.portfolio');
      const menu = document.querySelector('.header__switch');

      if (
        origin.index === 0 &&
        direction === 'down' &&
        portfolioActive.classList.contains('active')
      ) {
        fullpage_api.reBuild();
        filterSelection(categories.choose);
        submenu.classList.add('active');
        $(submenu)
          .find('a')
          .first()
          .addClass('active');
      } else if (
        direction === 'up' &&
        portfolioActive.classList.contains('active')
      ) {
        fullpage_api.reBuild();
        filterSelection(categories.choose);
        submenu.classList.add('active');
        $(submenu)
          .find('a')
          .first()
          .addClass('active');
        submenu.classList.add('active');
      } else if (origin.index === 1 && direction === 'up') {
        menu.classList.remove('open');
      } else if (origin.index === 0 && destination.index === 0) {
        menu.classList.remove('active');
      }
    },
  });
};

window.addEventListener('load', function() {
  const loader = document.querySelector('.loader');
  loader.className += ' hidden';
});

document.addEventListener('DOMContentLoaded', () => {
  // on Filtering
  filterSelection(categories.choose);
  $('#wybrane').on('click', function() {
    filterSelection(categories.choose);
    fullpage_api.reBuild();
  });
  $('#publiczne').on('click', function() {
    filterSelection(categories.publication);
    fullpage_api.reBuild();
  });
  $('#wielorodzinne').on('click', function() {
    filterSelection(categories.multifamily);
    fullpage_api.reBuild();
  });
  $('#przemysłowe').on('click', function() {
    filterSelection(categories.industrial);
    fullpage_api.reBuild();
  });
  $('#domy').on('click', function() {
    filterSelection(categories.houses);
    fullpage_api.reBuild();
  });

  //onFullPageJS
  if (window.innerWidth >= 768) {
    fullPGSDesktop();
  } else {
    fullPageScrollMobile();
  }

  //onSwiperSliders
  const portfolios = new SwiperCustomSlider('.portfolio__thumb');
  const medias = new SwiperCustomSlider('.media__thumb');
  const abouts = new SwiperCustomSlider('.about__thumb');

  if (window.innerWidth <= 992) {
    addClassActiveMenu();
  }
  switchMenu();
});
