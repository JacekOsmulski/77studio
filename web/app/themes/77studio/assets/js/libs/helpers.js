export const changeClassResize = (width = null, selector = '', removeClassName = '') => {
  if (window.innerWidth >= width) {
    document.querySelector(selector).classList.remove(removeClassName);
  } else {
    document.querySelector(selector).classList.add(removeClassName);
  }
};

export const changeMenuColor = (searchImg = '', searchClass = '', addClass = '', filters = []) => {
  const images = document.querySelectorAll(searchImg);
  for (let img of images) {
    let compStyles = window.getComputedStyle(img);
    let filtered = compStyles.getPropertyValue('background-image').split('/');
    let match = filtered[filtered.length - 1].slice(0, 6);
    for (let word of filters) {
      if (match === word) {
        document.querySelector(searchClass).classList.add(addClass);
      }
    }
  }
};
