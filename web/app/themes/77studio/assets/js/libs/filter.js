// Show filtered elements
export function addClass(element, name) {
  let i, arr1, arr2;
  arr1 = element.className.split(' ');
  arr2 = name.split(' ');
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) === -1) {
      element.className += ' ' + arr2[i];
    }
  }
}

// Hide elements that are not selected
export function removeClass(element, name) {
  let i, arr1, arr2;
  arr1 = element.className.split(' ');
  arr2 = name.split(' ');
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(' ');
}

export function filterSelection(classItem) {
  let allItems, i;
  allItems = document.getElementsByClassName('portfolio__thumb');
  for (i = 0; i < allItems.length; i++) {
    removeClass(allItems[i], 'show');
    addClass(allItems[i], 'animated');
    addClass(allItems[i], 'fadeIn');

    if (allItems[i].className.indexOf(classItem) > -1)
      addClass(allItems[i], 'show');
  }
}
