import $ from 'jquery';
class Tabs {
  constructor(container, tabs) {
    this.container = document.querySelector(container);
    this.tabs = document.querySelectorAll(tabs);

    this.prepareTabs();
  }

  prepareTabs() {
    this.tabs.forEach(tab => {
      tab.addEventListener('click', e => {
        e.preventDefault();
        const that = e.target;
        const href = that.getAttribute('href');
        tab.classList.remove('header__submenu-href--active');
        $(href).addClass('header__submenu-href--active');
        $('.portfolio__thumb').removeClass('portfolio__thumb--active');
        let id = href.split('#')[1];
        document.querySelectorAll(`.portfolio__thumb[data-category="${id}"]`).forEach(elem => {
          elem.classList.add('portfolio__thumb--active');
        });
      });
    });
  }
}

export default Tabs;
