class Scroller {
  constructor(rootSelector) {
    this.rootElement = document.querySelector(rootSelector);
    this.sections = document.querySelectorAll('section');
    const sectionsArray = [...this.sections];
    const currentSectionIndex = sectionsArray.findIndex(elem => this.isScrolledIntoView(elem));
    this.currentSectionIndex = Math.max(currentSectionIndex, 0);
    this.isThrottled = false;
    this.menuItems = document.querySelectorAll('.header__nav-link ');
  }

  expandMenuNav() {
    const headerSwitch = document.querySelector('.header__switch');
    headerSwitch.classList.add('open');
  }

  isScrolledIntoView(el) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;
    const isVisible = elemTop >= 0 && elemBottom <= Math.floor(window.innerHeight);

    return isVisible;
  }

  listenScroll(e) {
    if (this.isThrottled) return;
    this.isThrottled = true;

    setTimeout(() => {
      this.isThrottled = false;
    }, 1000);

    const direction = e.wheelDelta < 0 ? 1 : -1;
    this.scroll(direction);
  }

  scroll(direction) {
    if (direction === 1) {
      const isLastSection = this.currentSectionIndex === this.sections.length - 1;
      if (isLastSection) return;
    } else if (direction === -1) {
      const firstSection = this.currentSectionIndex === 0;
      if (firstSection) return;
    }
    this.currentSectionIndex = this.currentSectionIndex + direction;

    this.scrollToCurrentSection();
  }

  scrollToCurrentSection() {
    this.expandMenuNav();
    this.selectActiveNavItem();
    this.sections.forEach(section => section.classList.remove('active'));
    this.sections[this.currentSectionIndex].scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
    this.sections[this.currentSectionIndex].classList.add('active');
  }

  menuNavigation() {
    this.menuItems.forEach((menuItem, index) => {
      menuItem.classList.remove('header__nav-link--active');
      menuItem.addEventListener('click', e => {
        e.preventDefault();
        e.target.classList.add('header__nav-link--active');
        this.currentSectionIndex = index;
        this.scrollToCurrentSection();
      });
    });
  }

  selectActiveNavItem() {
    this.navigationContainer = document.querySelector('.header__nav-menu');

    if (this.navigationContainer) {
      this.menuNavigation();
      const navigationItems = this.navigationContainer.querySelectorAll('.header__nav-link');
      navigationItems.forEach((item, index) => {
        if (index === this.currentSectionIndex) {
          item.classList.add('header__nav-link--active');
        } else if (index === 0) {
          item.classList.remove('header__nav-link--active');
        } else {
          item.classList.remove('header__nav-link--active');
        }
      });
    }
  }
}

export default Scroller;
